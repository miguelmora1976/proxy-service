package net.tranzact.service;

import net.tranzact.domain.ProxyIpAddress;
import net.tranzact.repository.ProxyIpAddressRepository;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Created by mm186132 on 1/23/17.
 */
public class ProxyServiceTest {

    private ProxyIpAddressRepository proxyIpAddressRepository;
    
    private ProxyService proxyService;
    
    @Before
    public void setUp() {
        proxyIpAddressRepository = Mockito.mock(ProxyIpAddressRepository.class);
        
        ProxyIpAddress proxyIpAddress01 = new ProxyIpAddress();
        proxyIpAddress01.setId(1L);
        proxyIpAddress01.setIpAddress("192.168.1.1");
        proxyIpAddress01.setLastTimeUsed(DateTime.now().minusSeconds(16).toDate());
        
        ProxyIpAddress proxyIpAddress02 = new ProxyIpAddress();
        proxyIpAddress02.setId(2L);
        proxyIpAddress02.setIpAddress("192.168.1.2");
        proxyIpAddress02.setLastTimeUsed(DateTime.now().minusSeconds(8).toDate());
        
        List<ProxyIpAddress> proxyIpAddressList = new ArrayList<>();
        proxyIpAddressList.add(proxyIpAddress01);
        
        when(proxyIpAddressRepository.findAllAvailableProxies((Date)any())).thenReturn(proxyIpAddressList);
        when(proxyIpAddressRepository.findAll()).thenReturn(Arrays.asList(proxyIpAddress01, proxyIpAddress02));
        when(proxyIpAddressRepository.save((ProxyIpAddress)any())).thenReturn(proxyIpAddress01);
        proxyService = new ProxyService(proxyIpAddressRepository, 10);
    }
    
    @After
    public void tearDown(){
        proxyIpAddressRepository = null;
        proxyService = null;
    }
    
    @Test
    public void getProxies() {
        Assert.assertEquals(proxyService.getProxies().size(), 2);
    }
    
    @Test
    public void getProxy() {
        ProxyIpAddress pia = proxyService.getProxy();
        Assert.assertNotNull(pia);
        Assert.assertEquals(pia.getIpAddress(), "192.168.1.1");
    }
}