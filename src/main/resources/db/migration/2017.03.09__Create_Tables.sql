CREATE TABLE proxy_ip_addresses(
   id INT PRIMARY KEY NOT NULL,
   ip_address VARCHAR NOT NULL,
   last_time_used timestamp DEFAULT NULL,
   unique(ip_address)
);
