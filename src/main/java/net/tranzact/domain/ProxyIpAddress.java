package net.tranzact.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ProxyIpAddress entity object
 * Created by mm186132 on 03/09/17.
 */
@Entity
@Table(name="proxy_ip_addresses")
@Data
@NoArgsConstructor
public class ProxyIpAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", nullable = false)
    private Long id;
    
    @JsonProperty
    @Column(name="ip_address")
    private String ipAddress;
    
    @JsonProperty
    @Column(name="last_time_used", nullable = true)
    @Temporal(TemporalType.TIME)
    private Date lastTimeUsed;
    
    @JsonProperty
    @Transient
    private boolean available;
}
