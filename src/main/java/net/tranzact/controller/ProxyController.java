package net.tranzact.controller;

import javax.inject.Inject;

import lombok.Getter;
import net.tranzact.domain.ProxyIpAddress;
import net.tranzact.service.ProxyService;
import org.eclipse.jetty.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * ProxyController - RestController class for this web service
 */
@RestController
@RequestMapping(value = "/proxy", produces = MediaType.APPLICATION_JSON_VALUE)
@Getter
public class ProxyController {
    
    protected ProxyService proxyService;
    
    @Inject
    public ProxyController(ProxyService proxyService){
        this.proxyService = proxyService;
    }
    
    @RequestMapping(value = "/proxies", method = RequestMethod.GET)
    public List<ProxyIpAddress> getAllProxies() {
        return proxyService.getProxies();
    }

    @RequestMapping(value = "/process", method = RequestMethod.GET)
    public ResponseEntity getCustomer() {
        ProxyIpAddress proxyIpAddress = proxyService.getProxy();
        if (proxyIpAddress == null){
            return ResponseEntity.status(HttpStatus.PAYLOAD_TOO_LARGE_413).build();
        }
        return ResponseEntity.ok(proxyIpAddress);
    }
}
