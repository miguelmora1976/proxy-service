package net.tranzact.service;

import net.tranzact.domain.ProxyIpAddress;
import net.tranzact.repository.ProxyIpAddressRepository;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class ProxyService {
    private Logger logger = LoggerFactory.getLogger(ProxyService.class);
    protected ProxyIpAddressRepository proxyIpAddressRepository;
    @Value("${service.time.threshold}")
    private int threshold;
    
    @Inject
    public ProxyService(ProxyIpAddressRepository proxyIpAddressRepository){
        this.proxyIpAddressRepository = proxyIpAddressRepository;
    }
    
    @PostConstruct
    protected void process(){
        for (int i = 0; i < 100; i++){
            ProxyIpAddress proxyIpAddress = new ProxyIpAddress();
            proxyIpAddress.setIpAddress("192.168.1." + i);
            proxyIpAddressRepository.save(proxyIpAddress);
        }
    }
    
    public ProxyService(ProxyIpAddressRepository proxyIpAddressRepository, int threshold){
        this.proxyIpAddressRepository = proxyIpAddressRepository;
        this.threshold = threshold;
    }
    
    /**
     * Get all proxies in database
     * @return
     */
    public List<ProxyIpAddress> getProxies() {
        Iterator<ProxyIpAddress> iteratorToCollection = proxyIpAddressRepository.findAll().iterator();
    
        List<ProxyIpAddress> proxies = new ArrayList<>();
        
        Date now = DateTime.now().toDate();
        Calendar cal = Calendar.getInstance();

        cal.setTime(now);
        cal.add(Calendar.SECOND, -1 * threshold);
        Date delta = cal.getTime();
        while (iteratorToCollection.hasNext()){
            ProxyIpAddress pia = iteratorToCollection.next();

            if (pia.getLastTimeUsed() == null || pia.getLastTimeUsed().before(delta)){
                pia.setAvailable(true);
            }else{
                pia.setAvailable(false);
            }
            proxies.add(pia);
        }
    
        return proxies;
    }
    
    /**
     * Return a proxy IP address out of available proxies
     * @return
     */
    public ProxyIpAddress getProxy(){
        List<ProxyIpAddress> availableProxies = proxyIpAddressRepository.findAllAvailableProxies(deltaTime());
        
        if (availableProxies != null && availableProxies.size() > 0){
            logger.info(availableProxies.size() + " available proxies...");
            int randomElementIndex = ThreadLocalRandom.current().nextInt(availableProxies.size()) % availableProxies.size();
            ProxyIpAddress ipAddress = availableProxies.get(randomElementIndex);
            // Update the last accessed time
            ipAddress.setLastTimeUsed(DateTime.now().toDate());
            ipAddress.setAvailable(true);
            
            return proxyIpAddressRepository.save(ipAddress);
        }
        return null;
    }
    
    /**
     * Helper method to get delta time for comparison in the query
     * @return
     */
    private Date deltaTime(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis() - threshold * 1000L);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp.getTime());
        
        return cal.getTime();
    }
}
