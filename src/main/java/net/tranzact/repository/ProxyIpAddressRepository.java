package net.tranzact.repository;

import net.tranzact.domain.ProxyIpAddress;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ProxyIpAddressRepository extends CrudRepository<ProxyIpAddress, Long>{
    @Query("FROM ProxyIpAddress pia where pia.lastTimeUsed = null OR pia.lastTimeUsed < :threshold")
    List<ProxyIpAddress> findAllAvailableProxies(@Param("threshold") Date threshold);
}
