proxy-service

Summary

This repository is for the Proxy Service


Installation



The following should be downloaded and installed:



Git: a free and open source distributed version control system.

Java 8.


Setup Public SSH Authentication with Gitlab.
Clone this repository locally

$ git clone git@gitlab.com:miguelmora1976/proxy-service.git 

Navigate to the cloned location

$ cd ./proxy-service
Build the application

$ ./gradlew clean build

Run the Spring Boot application (setting server.port to 9000)

$ java -jar -Dserver.port=9000 build/libs/proxy-service.jar

Navigate to http://localhost:9000/swagger-ui.html to execute REST commands via the UI interface
